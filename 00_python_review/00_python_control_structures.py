# filename:     00_python_control_structures
# author:       dan.smith.me@gmail.com
# date:         05/08/2016
# version:      1.0
# =====================================================================================================================
import math

# for item in [1, 2, 3, 4, 5]:
#     print(item)
#
# wordlist = ['cat', 'dog', 'rabbit']
# letterlist = []
# for word in wordlist:
#     for aletter in word:
#         if aletter not in letterlist:
#             letterlist.append(aletter)
# print(letterlist)
#
# number = input("Please enter a number: ")
# if int(number) < 0:
#     print("sorry, value is negative")
# else:
#     print(math.sqrt(int(number)))

# List comprehensions

# we could do it this way
# squares_list = []
# for x in range(1, 11):
#     squares_list.append(x * x)
# print(squares_list)
#
# # however using list comprehension we can do it in one step:
# squares_list2 = [x * x for x in range(1, 11)]
# print(squares_list2)
#
# # we can also add selection criteria
# squares_list3 = [x * x for x in range(1, 11) if x % 2 != 0]
# print(squares_list3)

# by converting the list comprehension into a set and then back into a list
# we can remove duplicates very quickly and easily
# letter_list = list(set([word[i] for word in ['cat', 'dog', 'rabbit'] for i in range(len(word))]))
# print(letter_list)

# more list comprehensions

# numbers = [1, 2, 3, 4, 5]
#
# doubled_numbers = []
# for n in numbers:
#     doubled_numbers.append(n * 2)
#
# doubled_numbers2 = [n * 2 for n in numbers]
# print(doubled_numbers)
# print(doubled_numbers2)
#
# # for loop version
# doubled_odds = []
# for n in numbers:
#     if n % 2 == 1:
#         doubled_odds.append(n * 2)
#
# # list comprehension version
# doubled_odds2 = [n * 2 for n in numbers if n % 2 == 1]
#
# print(doubled_odds)
# print(doubled_odds2)
#

# matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
# flattened = []
# for row in matrix:
#     for n in row:
#         flattened.append(n)
#
# # brain wants this but it is wrong (flipped for loop
# # when making list comps the for should be in same order
#
# # flattened2 = [n for n in row for row in matrix] # NOT RIGHT!!
#
# flattened3 = [n for row in matrix for n in row]
# print(flattened == flattened3)
#
# words = ['cat', 'dog', 'rabbit']
# letters = list(set([ch for word in words for ch in word]))
# print(letters)

# # set comprehension
# first_letters = set()
# for w in words:
#     first_letters.add(w[0])
#
# first_letters2 = {w[0] for w in words}
# print(first_letters, first_letters2)

# flipping a dictionary
my_dict = {'a': 1, 'b': 2, 'c': 3}

flipped = {}
for key in my_dict:
    flipped[my_dict[key]] = key

flipped2 = {my_dict[key]: key for key in my_dict}

print(my_dict, flipped)
print(my_dict, flipped2)

# we can add line breaks between brackets
flipped2 = {my_dict[key]: key
            for key in my_dict
            }

numbers = [1, 2, 3, 4, 5, 6, 7, 8]
doubled_odds = [
    n * 2
    for n in numbers
    if n % 2 == 1
]


print(doubled_odds)