# filename:     00_python_input_output
# author:       dan.smith.me@gmail.com
# date:         05/08/2016
# version:      1.0
# =====================================================================================================================

# name = input('Please enter your name: ')
# print("Your name in capitals is ", name.upper(),
#       "and has length", len(name))
#
# # sradius = input("Please enter the radius of the circle: ")
# # radius = float(sradius)
# # diameter = 2 * radius
# # print("the diameter is", diameter)
#
# # String formatting
# print("%s is %d years old." % (name, 37))

# Modifiers

price = 24
item = 'banana'
print("The %s costs %d cents" % (item, price))
print("The %+10s costs %5.2f cents" % (item, price))
print("The %+10s costs %10.2f cents" % (item, price))

itemdict = {'item': 'banana', 'cost': 24}
print("The %(item)s costs %(cost)7.1f cents" % itemdict)