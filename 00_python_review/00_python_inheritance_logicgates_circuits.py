# filename:     00_python_inheritance_logicgates_circuits
# author:       dan.smith.me@gmail.com
# date:         10/08/2016
# version:      1.0
# =====================================================================================================================


class LogicGate:

    def __init__(self, n):
        self.name = n
        self.output = None

    def getName(self):
        return self.name

    def getOutput(self):
        self.output = self.performGateLogic()
        return self.output

class BinaryGate(LogicGate):

    def __init__(self, n):
        LogicGate.__init__(self, n)

        self.pinA = None
        self.pinB = None

    def getPinA(self):
        if not self.pinA:
            return int(input("Enter Pin A input for gate " + self.getName() + '-->'))
        else:
            return self.pinA.getFrom().getOutput()

    def getPinB(self):
        return int(input("Enter Pin B input for gate " + self.getName() + '-->'))

    def setNextPin(self, source):
        if not self.pinA:
            self.pinA = source
        else:
            if not self.pinB:
                self.pinB = source
            else:
                raise RuntimeError("Error: NO EMPTY PINS")


class UnaryGate(LogicGate):

    def __init__(self, n):
        LogicGate.__init__(self, n)

        self.pin = None

    def getPin(self):
        if not self.pin:
            return int(input("Enter Pin input for gate " + self.getName() + '-->'))
        else:
            return self.pin.getFrom().getOutput()

    def setNextPin(self, source):
        if not self.pin:
            self.pin = source
        else:
            print("Cannot Connect: NO EMPTY PINS on this gate")



class AndGate(BinaryGate):

    def __init__(self, n):
        super(AndGate, self).__init__(n)

    def performGateLogic(self):
        a = self.getPinA()
        b = self.getPinB()
        if a == 1 and b == 1:
            return 1
        else:
            return 0

class OrGate(BinaryGate):

    def __init__(self, n):
        super(OrGate, self).__init__(n)

    def performGateLogic(self):
        a = self.getPinA()
        b = self.getPinB()

        if a == 1 or b == 1:
            return 1
        else:
            return 0

class NotGate(UnaryGate):

    def __init__(self, n):
        UnaryGate.__init__(self, n)

    def performGateLogic(self):
        if self.getPin():
            return 0
        else:
            return 1

class Connector:

    def __init__(self, fgate, tgate):
        self.fromgate = fgate
        self.togate = tgate

        tgate.setNextPin(self)

    def getFrom(self):
        return self.fromgate

    def getTo(selfself):
        return self.togate



# g2 = OrGate("G2")
# result = g2.getOutput()
# print(result)
#
# g2 = OrGate("G2")
# result = g2.getOutput()
# print(result)
#
# g2 = OrGate("G2")
# result = g2.getOutput()
# print(result)
#
# g3 = NotGate("G3")
# result = g3.getOutput()
# print(result)
#
# g3 = NotGate("G3")
# result = g3.getOutput()
# print(result)

#  testing a circuit
g1 = AndGate("G1")
g2 = AndGate("G2")
g3 = OrGate("G3")
g4 = NotGate("G4")
c1 = Connector(g1, g3)
c2 = Connector(g2, g3)
c3 = Connector(g3, g4)

result = g4.getOutput()
print(result)