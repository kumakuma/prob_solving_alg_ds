# filename:     00_selfcheck_infinate_monkeys
# author:       dan.smith.me@gmail.com
# date:         08/08/2016
# version:      1.0
# =====================================================================================================================
import random

def random_string(n):
    alphabet = "abcdefghijklmnopqrstuvwxy "
    result = ''
    for i in range(n):
        result += alphabet[random.randint(0, len(alphabet) - 1)]

    return result


def check_sentence(astring, target):
    score = 0
    for i in range(len(astring)):
        if astring[i] == target[i]:
            score += 1
    return score


def generate_guesses(target):
    bestscore = 0
    tries = 0
    bestguess = ''
    while bestscore < 10000:
        guess = random_string(len(target))
        score = check_sentence(guess, target)
        if score > bestscore:
            bestscore = score
            bestguess = guess
        tries += 1
        if tries % 1000 == 0:
            print(bestguess + ' : ' + str(bestscore))

#
# target = 'methinks it is like a weasel'
#
# generate_guesses(target)


def random_character():
    alphabet = 'abcdefghijklmnopqrstuvwxyz '
    return alphabet[random.randint(0, len(alphabet) - 1)]


def better_check(astring, target):
    correctList = []
    score = 0
    for i in range(len(astring)):
        if astring[i] == target[i]:
            correctList.append([i, astring[i]])
            score += 1
    score = score / len(target) * 100
    return (score, correctList)


def semi_random_string(correctList, n):
    guess = ''
    guess_list = []
    for i in range(n):
        guess_list.append(random_character())

    for j in correctList:
        guess_list[j[0]] = j[1]

    for k in guess_list:
        guess += k

    return guess


def better_gen(target):
    bestscore = 0
    bestguess = ''
    correctList = []
    tries = 0
    while bestguess != target:
        guess = semi_random_string(correctList, len(target))
        checkresult = better_check(guess, target)
        score = checkresult[0]
        correctList = checkresult[1]

        if score > bestscore:
            bestscore = score
            bestguess = guess

        tries += 1
        if tries % 1 == 0:
            print(bestguess, bestscore)

#
# target = 'methinks it is like a weasel'
#
# better_gen(target)


# tutors version
import random

def generate_one(strlen):
    alphabet = 'abcdefghijklmnopqrstuvwxyz '
    result = ''
    for i in range(strlen):
        letter = alphabet[random.randrange(len(alphabet))]
        result += letter

    return result


def score(goal, teststring):
    numSame = 0
    for i in range(len(goal)):
        if goal[i] == teststring[i]:
            numSame += 1

    return numSame / len(goal)


def main():
    goalstring = 'methinks it is like a weasel'
    newstring = generate_one(len(goalstring))
    bestscore = 0
    bestguess = newstring
    newscore = score(goalstring, newstring)

    while newscore < 1:
        if newscore > bestscore:
            bestscore = newscore
            bestguess = newstring


        newstring = generate_one(len(goalstring))
        newscore = score(goalstring, newstring)

# target = 'methinks it is like a weasel'
print(score(target, generate_one(28)))


