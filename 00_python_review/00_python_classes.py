# filename:     00_python_classes
# author:       dan.smith.me@gmail.com
# date:         09/08/2016
# version:      1.0
# =====================================================================================================================


class Fraction:

    def __init__(self, top, bottom):
        """
        :param top: int defining the numerator
        :param bottom: int defining the denomiter
        """

        self.num = top
        self.den = bottom

    def __str__(self):
        return str(self.num) + '/' + str(self.den)

    def __add__(self, other):
        """
        :param other: Fraction instance
        :return: the sum of both fractions
        """
        newnum = self.num * other.den + self.den * other.num
        newden = self.den * other.den

        common = self.gcd(newnum, newden)
        return Fraction(newnum // common, newden // common)

    def gcd(self, m, n):
        while m % n != 0:
            oldm = m
            oldn = n

            m = oldn
            n = oldm % oldn
        return n

    def __eq__(self, other):
        firstnum = self.num * other.den
        secondnum = other.num * self.den

        return firstnum == secondnum

    def __mul__(self, other):
        newnum = self.num * other.num
        newden = self.den * other.den
        common = self.gcd(newnum, newden)

        return Fraction(newnum // common, newden // common)

    def __floordiv__(self, other):
        newnum = self.num * other.den
        newden = self.den * other.num
        common = self.gcd(newnum, newden)

        return Fraction(newnum // common, newden // common)

    def __truediv__(self, other):
        newnum = self.num * other.den
        newden = self.den * other.num
        common = self.gcd(newnum, newden)

        return Fraction(newnum // common, newden // common)

    def __sub__(self, other):
        newnum = self.num * other.den - other.num * self.den
        newden = self.den * other.den
        common = self.gcd(newnum, newden)
        return Fraction(newnum // common, newden // common)


myfraction = Fraction(3, 5)
print(myfraction)

f1 = Fraction(1, 4)
f2 = Fraction(1, 2)
f3 = f1 + f2
f4 = f1 * f2
f5 = f2 / f1
f6 = f1 - f2
f7 = f6 * f1
print(f3)
print(f4)
print(f5)
print(f6)
print(f7)