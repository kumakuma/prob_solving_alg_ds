# filename:     00_python_review
# author:       dan.smith.me@gmail.com
# date:         05/08/2016
# version:      1.0
# =====================================================================================================================

# # Built in atomic data types

# Arithmetic operators
# print(2+3*4)
# print((2+3)*4)
# print(2**10)
# print(6/3)
# print(7/3)
# print(7//3)
# print(7%3)
# print(3/6)
# print(3//6)
# print(3%6)
# print(2**100)

# # Boolean data type
# print(True)
# print(False)
# print(False or True)
# print(True and False)
# print(not True)

# # logical operators
# print(5==10)
# print(10 > 5)
# print((5 >= 1) and (5 <= 10))

# # Variables
# the_sum = 0
# print(the_sum)
#
# the_sum += 1
# print(the_sum)
#
# the_sum = True
# print(the_sum)

# # Built in collection types

# # lists
# mylist = [1024, 3, True, 6.5, False]
# # mylist.append(False) # makes pycharm complain about list literal
# print(mylist)
# mylist.insert(2, 4.5)
# print(mylist)
# print(mylist.pop())
# print(mylist)
# print(mylist.pop(1))
# print(mylist)
# mylist.pop(2)
# print(mylist)
# mylist.sort()
# print(mylist)
# mylist.reverse()
# print(mylist)
# print(mylist.count(6.5))
# print(mylist.index(4.5))
# mylist.remove(6.5)
# print(mylist)
# del mylist[0]
# print(mylist)

# # range function
# range(10)
# list(range(10))
# range(5, 10)
# list(range(5, 10))
# list(range(5, 10, 2))
# list(range(10, 1, -1))

# # Strings
# my_name = "Daniel"
# print(my_name[3])
# print(my_name * 2)
# len(my_name)
# print(my_name.upper())
# print(my_name)
# print(my_name.find('n'))
# print(my_name.center(15))
# print(my_name.split('n'))

# # Tuples
# my_tuple = ('Dan', 37)
# print(my_tuple)
# print(my_tuple[1])
# print(my_tuple * 3)
# my_tuple[0] = False # illegal method (not mutable)

# # Sets
# myset = {3, 6, 'cat', 4.5, False}
# yourset = {99, 3, 100}
# print(myset.union(yourset))
# print(myset | yourset)
# print(myset)
# print(yourset)
# print(myset.intersection(yourset))
# print(myset & yourset)
# print(myset.difference(yourset))
# print({3, 100}.issubset(yourset))
# myset.add('house')
# print(myset)
# print(myset.pop())
# print(myset)
# myset.clear()
# print(myset)

# # dictionaries
# capitals = {'England': 'London', 'France': 'Paris', 'Germany': 'Berlin', 'Japan': 'Tokyo'}
# print(capitals['Japan'])
# capitals['Italy'] = 'Rome'
# for k in capitals:
#     print(capitals[k], "is the capital of", k)
#
# print('*'*10, '\n')
# print(capitals.keys())
# print(capitals.values())
# print(list(capitals.values()))
# print(capitals.get('Russia'))
# print(capitals.get('Russia', 'No Entry Found'))