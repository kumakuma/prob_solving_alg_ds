# filename:     00_python_exception_handling
# author:       dan.smith.me@gmail.com
# date:         08/08/2016
# version:      1.0
# =====================================================================================================================
import math

anumber = int(input("Please enter an integer: "))
try:
    print(math.sqrt(anumber))
except:
    print("Bad value for square root")
    print("Using absolute value instead")
    print(math.sqrt(abs(anumber)))

if anumber < 0:
    raise RuntimeError("You can't use a negative number!")
else:
    print(math.sqrt(anumber))